# FreePost

This is the code powering [freepost](http://freepo.st). FreePost is a web-based discussion board that allows users to post text and links which other users may read and comment on (start a discussion). It also supports upvoting and downvoting of posts and has some nifty features to display the so-called **Hot** posts (those that are very popular and have been upvoted a lot) as well as the newest posts, aptly named **New**. Each user has a profile page which includes some information on themselves.

## Development

### Stylesheets

Sources are in `css/`. They are compiled using Stylus. Run this command from the project's root:

`$ stylus --watch --compress --disable-cache --out css/ css/`

## Contacts

If you have any questions please get in contact with us at [freepost](http://freepo.st).

## License

freepost is [free software](https://www.gnu.org/philosophy/free-sw.html) licensed as GNU Affero General Public License, either version 3 or (at your option) any later version.
